#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UITextFieldDelegate>{
    NSString *errorLog;
    }

@property (nonatomic, retain) IBOutlet UITextField *emailTextField;
@property (nonatomic, retain) IBOutlet UITextField *passwordTextField;
@property (nonatomic, retain) IBOutlet UITextField *repeatPasswordTextField;


- (IBAction)pushSumbit:(id)sender;
-(BOOL) validEmail:(NSString*) emailString;
@end
