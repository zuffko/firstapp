#import "ViewController.h"
@interface ViewController ()
@end
@implementation ViewController
@synthesize emailTextField, passwordTextField, repeatPasswordTextField;

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.passwordTextField.delegate = self;
    self.repeatPasswordTextField.delegate = self;
    self.emailTextField.delegate = self;
}

- (IBAction)pushSumbit:(id)sender;
{
    [repeatPasswordTextField resignFirstResponder];
    [emailTextField resignFirstResponder];
    [passwordTextField resignFirstResponder];
    
    NSString *email = emailTextField.text;
    NSString *password = passwordTextField.text;
    NSString *passwordConfirmation = repeatPasswordTextField.text;
    
    if ([self validEmail: email ]== YES){
    } else {
        errorLog = @"Email not valid";
        }
    
    if ([password isEqualToString: passwordConfirmation]){
    }else{
        if ([errorLog length]<1) {
            errorLog = @"pass not equal";
        }else{
        errorLog = [errorLog stringByAppendingString:@", "];
        errorLog = [errorLog stringByAppendingString:@"pass not equal"];
        }
    }
        
    if ([errorLog length]>0){
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Data verification" message: errorLog delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        [alert release];
    }else{
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Data verification" message: @"All ok" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        [alert release];
    }

}

-(BOOL) validEmail:(NSString*) emailString {
    NSString *regExPattern = @"^[A-Z0-9._%+-]+@[A-Z0-9.-]+.[A-Z]{2,4}$";
    NSRegularExpression *regEx = [[NSRegularExpression alloc] initWithPattern:regExPattern options:NSRegularExpressionCaseInsensitive error:nil];
    NSUInteger regExMatches = [regEx numberOfMatchesInString:emailString options:0 range:NSMakeRange(0, [emailString length])];
    NSLog(@"%i", regExMatches);
    if (regExMatches == 0) {
        return NO;
    } else
        return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)aTextField{
    
    if (aTextField == emailTextField) {
             [emailTextField resignFirstResponder];
             [passwordTextField becomeFirstResponder];
         }
         else if (aTextField == passwordTextField){
             [passwordTextField resignFirstResponder];
             [repeatPasswordTextField becomeFirstResponder];
         }
         else if (aTextField == repeatPasswordTextField){
             [repeatPasswordTextField resignFirstResponder];
         }
    return YES;
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}


- (void) dealloc
{
    [emailTextField release];
    [repeatPasswordTextField release];
    [passwordTextField release];
    [super dealloc];

}
 
@end
